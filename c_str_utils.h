/* 
 * File:   c_str_utils.h
 * @author pharo
 *
 * Created on 13 lipca 2015, 22:00
 */

#ifndef C_STR_UTILS_H
#define	C_STR_UTILS_H

#ifdef	__cplusplus
extern "C" {
#endif

char** strsplit(const char*,const char*,unsigned int*);

void array_free(void** arr, int size);

#ifdef	__cplusplus
}
#endif

#endif	/* C_STR_UTILS_H */

