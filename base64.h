#include <inttypes.h>
#include <string.h>

int base64decode (char *in, size_t inLen, unsigned char *out, size_t *outLen);
int base64encode(const void* data_buf, size_t dataLength, char* result, size_t resultSize);
