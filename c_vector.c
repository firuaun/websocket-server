#include <stddef.h>
#include <malloc.h>
#include <stdbool.h>
#include "c_vector.h"


c_vector* __c_vector_init(void* value){
    c_vector* vec = malloc(sizeof(c_vector));
    vec->list = NULL;
    vec->prev = NULL;
    vec->next = NULL;
    vec->value = value;
    return vec;
}

c_vector_list* __c_vector_list_init(c_vector* vec){
    c_vector_list* list = malloc(sizeof(c_vector_list));
    list->head = vec;
    list->tail = vec;
    list->size = 0;
    return list;
}

c_vector* c_vector_new(void* value) {
    c_vector* vec = __c_vector_init(value);
    c_vector_list* list = __c_vector_list_init(vec);
    vec->list = list;
    vec->list->size++;
    return vec;
}

c_vector* c_vector_push(c_vector* vec, void* value) {
    c_vector* last = vec->list->tail;
    c_vector* append = __c_vector_init(value);
    append->prev = last;
    append->list = vec->list;
    vec->list->tail = append;
    last->next = append;
    vec->list->size++;
    return append;
}

c_vector* c_vector_pop(c_vector* vec) {
    c_vector* last = vec->list->tail;
    c_vector* prev = last->prev;
    if(prev != NULL) {
        vec->list->tail = prev;
        vec->list->size--;
        prev->next = (c_vector*)NULL;
        last->prev = (c_vector*)NULL;
        last->list = (c_vector_list*) NULL;
    }
    return last;
}

c_vector* c_vector_get(c_vector* vec, unsigned int index) {
    int tmp_i;
    c_vector* tmp;
    int total_size = c_vector_size(vec);
    if(index >= total_size)
        return (c_vector*)NULL;
    if(index > total_size/2) {
        tmp  = vec->list->tail;
        tmp_i = total_size-1;
        while(tmp_i > index) {
            tmp = tmp->prev;
            tmp_i--;
        }        
    }
    else {
        tmp  = vec->list->head;
        tmp_i = 0;
        while(tmp_i < index) {
            tmp = tmp->next;
            tmp_i++;
        }
    }
    return tmp;
}

c_vector* c_vector_remove(c_vector** vec, int index){
    c_vector* found;
    if(index == CV_REMOVE_GIVEN_VECTOR)
        found = *vec;
    else
        found = c_vector_get(*vec,index);
    if(found == NULL)
        return found;
    c_vector* next = found->next;
    c_vector* prev = found->prev;
    bool is_first = prev == NULL;
    bool is_last = next == NULL;
    if(is_first && is_last)
        return found;
    if(!is_last) {
        next->prev = prev;
        found->next = NULL;
    }
    else {
        found->list->tail = prev;
    }
    if(!is_first) {
        prev->next = next;
        found->prev = NULL;
    }
    else {
        found->list->head = next;
    }
    *vec = found->list->head;
    found->list->size--;
    found->list = NULL;
    return found;
}

c_vector* c_vector_unshift(c_vector* vec, void* value) {
    c_vector* first = vec->list->head;
    c_vector* append = __c_vector_init(value);
    append->next = first;
    append->list = vec->list;
    vec->list->head = append;
    first->prev = append;
    vec->list->size++;
    return append;
}

c_vector* c_vector_shift(c_vector* vec) {
    c_vector* first = vec->list->head;
    c_vector* next = first->next;
    if(next != NULL) {
        vec->list->head = next;
        vec->list->size--;
        next->prev = (c_vector*)NULL;
        first->next = (c_vector*)NULL;
        first->list = (c_vector_list*) NULL;
    }
    return first;
}

inline int c_vector_size(c_vector* vec){
    return vec->list->size;
}


inline void __c_vector_free_single(c_vector* vec) {
    free(vec);
}

void c_vector_free(c_vector* vec){
    if(vec == NULL)
        return;
    c_vector_list* list = vec->list;
    c_vector_foreach(vec, &__c_vector_free_single);
    free(list);
}

void** c_vector_to_array(c_vector* vec){
    c_vector* tmp = vec->list->head;
    int length = vec->list->size;
    void** array = (void**) malloc(sizeof(void*)*length);
    int i;
    for(i=0; i < length; i++) {
        *(array+i) = tmp->value;
        tmp = tmp->next;
    }
    return array;
}

void c_vector_foreach(c_vector* vec, void (*fun)(c_vector*)) {
    c_vector *next, *tmp;
    if(vec == NULL)
        return;
    next = vec->list->head;
    do {
        tmp = next;
        next = next->next;
        (*fun)(tmp);
    }while(next != NULL);
}