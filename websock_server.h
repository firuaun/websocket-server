#ifndef WEBSOCK_SERVER_H
#define	WEBSOCK_SERVER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "base64.h"
#include "c_str_utils.h"
#include <string.h>
#include <openssl/sha.h>
#include <stdbool.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>


#define WEBSOCKET_REQUEST_HEADER_KEY "Sec-WebSocket-Key"
#define WEBSOCKET_MAGIC_KEY "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define WEBSOCKET_MACIG_KEY_LENGTH 36

#define RETURN_ERROR_CODE -1

typedef struct {
	int sock;
	struct sockaddr_in server;
} wss_server;

typedef struct {
	int sock;
	struct sockaddr client;
} wss_client;

void websock_magic_key(const char* recv, size_t recv_length, char* out, size_t out_length);

bool simpleSHA256(void* input, unsigned long length, unsigned char* md);

void get_magick_key_seed(char* request, char* out);

void webserver_get_header_by_name(char* request, char* header_key, char *out);

int wss_create(const char* ip_addr, int port_num, wss_server* srv);

int wss_accept(const wss_server* srv, wss_client* cln);

int __wss_handshake(const wss_server* srv, const wss_client* cln);

int wss_recv(const wss_client* cln, char* out);

int wss_send(const wss_client* cln, const char* in);

#ifdef	__cplusplus
}
#endif

#endif	/* WEBSOCK_SERVER_H */

