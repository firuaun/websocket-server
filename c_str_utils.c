#include "c_str_utils.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "c_vector.h"

char** strsplit(const char* string, const char* delimiter, unsigned int* return_size) {
    void** result;
    c_vector* list = c_vector_new(CV_RAW_PARAM(int,0));
    unsigned int del_len = strlen(delimiter);
    unsigned int i = 0;
    unsigned int from = 0;
    bool is_eos = false;
    do{
        is_eos = *(string+i)=='\0';
        if(*(string+i)==delimiter[0] || is_eos) {
            if(strncmp(string+i,delimiter,del_len) == 0 || is_eos) {
                int length = i-from;
                if(length != 0) {
                    char* buffer = (char*) malloc(length+1);
                    strncpy(buffer,string+from,length);
                    buffer[length]='\0';
                    list = c_vector_push(list,CV_VAR_PARAM(*buffer));
                }
                from = i+del_len;
            }
        }
        i++;
    }while(!is_eos);
    free(c_vector_shift(list));
    result = c_vector_to_array(list);
    *return_size = c_vector_size(list); 
    c_vector_free(list);
    return (char**) result;
}

void array_free(void** arr, int size){
    int i;
    for(i=0; i < size; i++){
        free(*(arr+i));
    }
    free(arr);
}