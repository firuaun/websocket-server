#include "websock_server.h"

void websock_magic_key(const char* recv, size_t recv_length, char* out, size_t out_length){
	int buff_length = recv_length + WEBSOCKET_MACIG_KEY_LENGTH + 1;
	char buffer[buff_length];
	unsigned char sha_buffer[SHA_DIGEST_LENGTH];
	memcpy(buffer,recv,recv_length);
	memcpy(buffer+recv_length,WEBSOCKET_MAGIC_KEY,WEBSOCKET_MACIG_KEY_LENGTH);
	buffer[buff_length-1] = '\0';
	SHA1((const unsigned char*)buffer, buff_length-1 ,sha_buffer);
	base64encode(sha_buffer,SHA_DIGEST_LENGTH,out,out_length);
}

bool simpleSHA256(void* input, unsigned long length, unsigned char* md){
    SHA256_CTX context;
    if(!SHA256_Init(&context))
        return false;

    if(!SHA256_Update(&context, (unsigned char*)input, length))
        return false;

    if(!SHA256_Final(md, &context))
        return false;

    return true;
}

void get_magick_key_seed(char* request, char* out) {
	webserver_get_header_by_name(request, WEBSOCKET_REQUEST_HEADER_KEY, out);
}

void webserver_get_header_by_name(char* request, char* header_key, char *out) {
	unsigned int header_count;
	int magic_key_req_key_length = strlen(header_key),i;
	char** splited_headers = strsplit(request,"\r\n",&header_count);

	for(i=0; i < header_count; i++){
		char* header = *(splited_headers+i);
		if(strncmp(header,header_key,magic_key_req_key_length) == 0) {
			unsigned int l;
			char** tmp = strsplit(header,": ",&l);
			strcpy(out,*(tmp+1));
			array_free((void**)tmp,l);
		}
	}
	array_free((void**)splited_headers,header_count);
}

int wss_create(const char* ip_addr, int port_num, wss_server* srv) {
	srv->sock = socket(AF_INET, SOCK_STREAM, 0);
	srv->server.sin_family = AF_INET;
	srv->server.sin_addr.s_addr = inet_addr(ip_addr);
	srv->server.sin_port = htons(port_num);
	if(bind(srv->sock, (struct sockaddr*)&srv->server, sizeof(srv->server)) < 0){
		return RETURN_ERROR_CODE;
	}
	return listen(srv->sock,1);
}

int wss_accept(const wss_server* srv, wss_client* cln) {
	socklen_t len = sizeof(struct sockaddr);
	cln->sock = accept(srv->sock,&cln->client,&len);
	return __wss_handshake(srv,cln);
}

#define REQUEST_BUFFER 600

#define MAGICKEY_BUFFER_SIZE 50

#define WEBSERVER_RESPONSE_TEMPLATE "HTTP/1.1 101 Switching Protocols\r\n\
Upgrade: websocket\r\n\
Connection: Upgrade\r\n\
Sec-WebSocket-Accept: %s\
\r\n\r\n"

int __wss_handshake(const wss_server* srv, const wss_client* cln) {

	char buffer[REQUEST_BUFFER], response_buffer[REQUEST_BUFFER];
	char magickey_seed[MAGICKEY_BUFFER_SIZE], magickey[MAGICKEY_BUFFER_SIZE];

	bzero(buffer,REQUEST_BUFFER);
	if(recv(cln->sock,buffer,REQUEST_BUFFER,0) < 0) {
		return RETURN_ERROR_CODE;
	}

	get_magick_key_seed(buffer,magickey_seed);

	websock_magic_key(magickey_seed,strlen(magickey_seed),magickey,MAGICKEY_BUFFER_SIZE);

	bzero(response_buffer,REQUEST_BUFFER);

	sprintf(response_buffer,WEBSERVER_RESPONSE_TEMPLATE,magickey);

	return send(cln->sock,response_buffer,strlen(response_buffer),0);
}

int wss_recv(const wss_client* cln, char* out) {
	char buffer[REQUEST_BUFFER];
	unsigned short fin;
	unsigned short mask_flag;
	unsigned long length;

	const char* mask;
	const char* data;

	int i;

	if(recv(cln->sock,buffer,REQUEST_BUFFER,0) < 0) {
		return RETURN_ERROR_CODE;
	}

	fin = (buffer[0] & 128) >> 7;
	mask_flag = (buffer[1] & 128) >> 7;
	length = buffer[1] & 127;

	if(length == 126) {
		length = (uint16_t) buffer[2];
		mask = buffer+6;
		data = buffer+10;
	}
	else if(length == 127) {
		length = (uint64_t) buffer[2];
		mask = buffer+10;
		data = buffer+14;
	}
	else {
		mask = buffer+2;
		data = buffer+6;
	}

	for(i=0; i < length; i++) {
		out[i] = data[i] ^ mask[i%4];
	}
	out[i] = '\0';
	return 1;

}

int wss_send(const wss_client* cln, const char* in) {
	char buffer[REQUEST_BUFFER];
	int i;
	unsigned long wlen, len = strlen(in);
	char* mask, *data;

	bzero(buffer,REQUEST_BUFFER);

	buffer[0] = 1 << 7 | 1;
	if(len < 128) {
		buffer[1] = 1 << 7 | len;
		wlen = len+6;
		mask = buffer+2;
		data = buffer+6;
	}
	else if(len < 1 << 16){
		buffer[1] = 1 << 7 | 126;
		buffer[3] = len;
		wlen = len+10;
		mask = buffer+6;
		data = buffer+10;
	}
	else {
		buffer[1] = 128;
		buffer[3] = len;
		wlen = len+14;
		mask = buffer+10;
		data = buffer+14;
	}

	for(i=0; i < len; i++) {
		data[i] = in[i] ^ mask[i%4];
	}
	data[i] = '\0';

	return send(cln->sock, buffer, wlen, 0);
}