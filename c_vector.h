/* 
 * File:   c_vector.h
 * @author pharo
 *
 * Created on 12 lipca 2015, 18:14
 */

#ifndef C_VECTOR_H
#define	C_VECTOR_H

#ifdef	__cplusplus
extern "C" {
#endif

    
#define CV_STRING(value) ((char*)value)
#define CV_VALUE(type, value) *((type*)value)
#define CV_ARRAY_VALUE(type, value, i) *(((type*)value)+i)
#define CV_VAR_PARAM(value) (void*) &value
#define CV_RAW_PARAM(type,value) (void*) &(type){value}
    
#define CV_REMOVE_GIVEN_VECTOR -1
    
typedef struct c_vector c_vector;
typedef struct c_vector_list c_vector_list;
    
struct c_vector{
    c_vector* next;
    c_vector* prev;
    c_vector_list* list;
    void* value;
};    

struct c_vector_list{
    c_vector* head;
    c_vector* tail;
    int size;
};

c_vector* __c_vector_init(void*);

c_vector_list* __c_vector_list_init(c_vector*);

/**
	Initialization of vector list
	@param value Value of the first element of vector
	@returns First element
*/
c_vector* c_vector_new(void*);

/**
	Appends element at the end of list with given value
	@param vec Any element that belongs to the list
	@param value Value of the new element
	@returns The created element that now is the last on the list
*/
c_vector* c_vector_push(c_vector*, void*);

/**
        Gets and removes the last element from the list
	@param vec Any element that belongs to the list
	@returns The last element from the list (you need free it manually after, bc c_vector_free wont work on it)
*/
c_vector* c_vector_pop(c_vector*);

/**
	Gets element from the list under given index counting from the begining otherwise NULL
	@param vec Any element that belongs to the list
	@param index Index of desired element
	@returns Element which can be found under given index or NULL if inappropriate index given (not within the range 0...size)
*/
c_vector* c_vector_get(c_vector*, unsigned int);

/**
	Gets and removes element from the list under given index counting from the begining otherwise NULL
	@param vec Addres of pointer to the element that belongs to the list, might be changed to head element of the list
	@param index Index of desired element or CV_REMOVE_GIVEN_VECTOR (-1) which means that current element is going to be removed
	@returns Element which can be found under given index or NULL if inappropriate index given (not within the range 0...size) or -1
*/
c_vector* c_vector_remove(c_vector**, int);

/**
	Appends element at the beginning of list with given value
	@param vec Any element that belongs to the list
	@param value Value of the new element
	@returns The created element that now is first on the list
*/
c_vector* c_vector_unshift(c_vector*, void*);

/**
        Gets and removes the first element from the list
	@param vec Any element that belongs to the list
	@returns The first element from the list (you need free it manually after, bc c_vector_free wont work on it)
*/
c_vector* c_vector_shift(c_vector*);

/**
	Get current size of list
	@param vec Any element that belongs to the list
	@returns Size of vectors list
*/
inline int c_vector_size(c_vector*);

inline void __c_vector_free_single(c_vector*);

/**
	Essential operation to clear the memory
	@param vec Any element that belongs to the list
*/
void c_vector_free(c_vector*);

void** c_vector_to_array(c_vector*);

/**
        Iterate through all elements of vector, which given function as second parameter
	@param vec Any element that belongs to the list
	@param fun A pointer to the function that returns void and has pointer to vector as first and only parameters
*/
void c_vector_foreach(c_vector*, void (*fun)(c_vector*));

#ifdef	__cplusplus
}
#endif

#endif	/* C_VECTOR_H */

