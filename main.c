#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include "c_str_utils.h"
#include "websock_server.h"


#define DEFAULT_HOST "127.0.0.1"
#define DEFAULT_PORT 44405

#define BUFFER_SIZE 600
#define MAGICKEY_BUFFER_SIZE 50


int main(int argc, char* argv[]){

	
	wss_server server;
	wss_client client;
	char buffer[MAGICKEY_BUFFER_SIZE];

	if(wss_create(DEFAULT_HOST,DEFAULT_PORT,&server) < 0){
		printf("Couldn't create server\n");
		return 1;
	}

	if(wss_accept(&server,&client) < 0) {
		printf("Couldn't accept connection\n");
		return 1;
	}
	
	bzero(buffer,MAGICKEY_BUFFER_SIZE);

	if(wss_recv(&client, buffer) < 0) {
		printf("Waiting to read anything failed\n");
		return 1;
	}

	printf("Got message: %s\n",buffer);

	if(wss_send(&client, "test roger") < 0) {
		printf("Send failed\n");
		return 1;
	}

	if(wss_recv(&client, buffer) < 0) {
		printf("Waiting to read anything failed\n");
		return 1;
	}

	printf("Got message: %s\n",buffer);

	close(client.sock);

	close(server.sock);
	
	return 0;
}

